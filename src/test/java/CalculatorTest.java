import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
/*
class
CalculatorTest {
	@Test
	void testAdd()
	{
		Calculator calculator =	new	Calculator();
		assertEquals(4, calculator.add(2,2),"2 + 2 = 4"
		);
	}
	@RepeatedTest(5)
	void RepeatTest(){
		assertEquals(6);
	}
}*/


class CalculatorTest {
	private Calculator calculator = new Calculator();

	/*@BeforeAll
	void PrintBeforeAll() {
		System.out.println("Nu körs BeforeAll");
	}*/

	/*@BeforeEach
	void ResetCalculator() {
		calculator = new Calculator();
	}*/

	@Test
	void CalculatorTest1() {
		Assertions.assertEquals(4, calculator.add(2, 2), "2 + 2 = 4");
	}
	@Test
	void CalcTest(){
		Assertions.assertEquals(4,calculator.add(2,2));
	}

	@RepeatedTest(5)
	void CalculatorTest2() {
		Assertions.assertEquals(6, calculator.add(2, 4), "2 + 4 = 6");
	}

	/*@Test
	void FailTest() {
		try {
			calculator.add(1,1);
			fail("Exception wasn't thrown!");
			}
		catch (NullPointerException  exception){
			
		}
	}*/
}
